package com.galvanize;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ZipCodeProcessorTest {

    ZipCodeProcessor zipCodeProcessor;
    Verifier verifier;

    @BeforeEach
    void setUp() {
        verifier = new Verifier();
        zipCodeProcessor = new ZipCodeProcessor(verifier);
    }

    // write your tests here

    @Test
    void returnsConfirmationMessage() {
        String actual = zipCodeProcessor.process("97237");
        String expected = "Thank you!  Your package will arrive soon.";

        assertEquals(expected, actual);
    }

    @Test
    void returnsInvalidFormatMessage() {
        String actual = zipCodeProcessor.process("97237789");
        String expected = "The zip code you entered was the wrong length.";

        assertEquals(expected, actual);
    }

    @Test
    void returnsNoServiceMessage() {
        String actual = zipCodeProcessor.process("19852");
        String expected = "We're sorry, but the zip code you entered is out of our range.";

        assertEquals(expected, actual);
    }
}