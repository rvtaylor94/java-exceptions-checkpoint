package com.galvanize;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class VerifierTest {

    Verifier verify;
    @BeforeEach
    void setUp() {
        verify = new Verifier();
    }

    @Test
    void throwsWhenLengthTooLong() {
        InvalidFormatException formatException = assertThrows(InvalidFormatException.class, () ->
                verify.verify("321510215"));

        assertEquals("ERRCODE 21: INPUT_TOO_LONG", formatException.getMessage());
    }

    @Test
    void throwsWhenLengthTooShort() {
        InvalidFormatException formatException = assertThrows(InvalidFormatException.class, () ->
                verify.verify("526"));

        assertEquals("ERRCODE 22: INPUT_TOO_SHORT", formatException.getMessage());
    }
    @Test
    void throwsWhenStartsWith1() {
        NoServiceException formatException = assertThrows(NoServiceException.class, () ->
                verify.verify("12345"));

        assertEquals("ERRCODE 27: NO_SERVICE", formatException.getMessage());
    }

}